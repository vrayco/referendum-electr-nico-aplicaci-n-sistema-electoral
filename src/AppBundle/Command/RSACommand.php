<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RSACommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('referendumelectronico:rsa:generar_claves')
            ->setDescription('Genera ó actualiza el par de claves RSA propias del servidor');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('monolog.logger.auditoria');
        $rsa    = $this->getContainer()->get('rsa');

        if($rsa->generarClaves()) {
            $msg = "Se han creado/actualizado las claves RSA del sistema.";
            $logger->info($msg);
            $output->writeln('<info>'.$msg.'</info>');
        } else {
            $msg = "Ha fallado la creación/generación de claves RSA del sistema.";
            $logger->error($msg);
            $output->writeln('<error>'.$msg.'</error>');
        }

    }
}
