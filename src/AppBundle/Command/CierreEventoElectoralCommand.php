<?php

namespace AppBundle\Command;

use AppBundle\Entity\EventoElectoral;
use AppBundle\Entity\L1;
use AppBundle\Entity\Papeleta;
use AppBundle\Entity\Voto;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CierreEventoElectoralCommand extends ContainerAwareCommand
{
    const NUM_SHUFFLE = 5;

    protected function configure()
    {
        $this
            ->setName('referendumelectronico:eventoelectoral:cierre')
            ->setDescription('Realiza las tareas necesarias para cerrar el evento electoral.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger = $this->getContainer()->get('monolog.logger.auditoria');

        $eventos = $em->getRepository('AppBundle:EventoElectoral')->findBy(array('estado' => EventoElectoral::ESTADO_CERRADO));

        foreach ($eventos as $evento) {

            $logger->info(
                sprintf("[EVENTO %s] Operaciones de cierre del proceso electoral inciadas.", $evento->getId())
            );

            $votosValidadosDesordenados = new \Doctrine\Common\Collections\ArrayCollection();
            $votosValidadosDesordenados2= new \Doctrine\Common\Collections\ArrayCollection();
            $votosValidados = new \Doctrine\Common\Collections\ArrayCollection();

            $votos = $evento->getVotos();
            $logger->info(
                sprintf("[EVENTO %s] Número total de votos %d.", $evento->getId(), $votos->count())
            );

            // FILTRAMOS LOS VOTOS VALIDADOS Y DESCARTAMOS EL RESTO
            foreach ($votos as $v)
                if($v->getEstado() === Voto::ESTADO_VALIDADO)
                    $votosValidados->add($v);

            $logger->info(
                sprintf("[EVENTO %s] Número total de votos validados %d.", $evento->getId(), $votosValidados->count())
            );

            // Desordenamos los votos
            $indices = $votosValidados->getKeys();
            for($i = 0; $i < self::NUM_SHUFFLE; $i++)
                shuffle($indices);
            foreach ($indices as $i)
                $votosValidadosDesordenados->add($votosValidados->get($i));

            // Guardo papeletas
            foreach ($votosValidadosDesordenados as $v) {
                $papeleta = new Papeleta();
                $papeleta->setVPrima($v->getVPrima());
                $papeleta->setVotaciones($v->getVotaciones());
                $papeleta->setEventoElectoral($evento);
                $em->persist($papeleta);
            }

            $logger->info(
                sprintf("[EVENTO %s] Guardamos papeltas (aleatoriamente).", $evento->getId())
            );

            // Desordenamos los votos
            $indices = $votosValidados->getKeys();
            for($i = 0; $i < self::NUM_SHUFFLE; $i++)
                shuffle($indices);
            foreach ($indices as $i)
                $votosValidadosDesordenados2->add($votosValidados->get($i));

            // Guardo l1s
            foreach ($votosValidadosDesordenados2 as $v) {
                $l1 = new L1();
                $l1->setL1($v->getL1());
                $l1->setEventoElectoral($evento);
                $em->persist($l1);
            }

            $logger->info(
                sprintf("[EVENTO %s] Guardamos los localizadores L1 (aleatoriamente).", $evento->getId())
            );

            // Eliminamos los votos de la urna
            foreach ($votos as $v)
                $em->remove($v);

            $logger->info(
                sprintf("[EVENTO %s] Eliminanos todos los votos.", $evento->getId())
            );

            // Cambio el estado del evento a FINALIZADO
            $evento->setEstado(EventoElectoral::ESTADO_FINALIZADO);

            $logger->info(
                sprintf("[EVENTO %s] Operaciones de cierre del proceso electoral terminadas.", $evento->getId())
            );
        }

        $em->flush();
    }
}