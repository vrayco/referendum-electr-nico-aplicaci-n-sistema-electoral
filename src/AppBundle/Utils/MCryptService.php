<?php

namespace AppBundle\Utils;

use Symfony\Component\Config\Definition\Exception\Exception;

class MCryptService
{
    private $iv = null; //'fedcba9876543210'; #Same as in JAVA
    private $key = null;//'0123456789abcdef'; #Same as in JAVA


    function __construct() {}

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function setIV($iv)
    {
        $this->iv = $iv;
    }

    function encrypt($str) {

        //$key = $this->hex2bin($key);
        $iv = $this->iv;

        $td = mcrypt_module_open('rijndael-128', '', 'cbc', $this->iv);

        mcrypt_generic_init($td, $this->key, $this->iv);
        $encrypted = mcrypt_generic($td, $str);

        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        return bin2hex($encrypted);
    }

    function decrypt($code) {

        $code = $this->hex2bin($code);

        $td = mcrypt_module_open('rijndael-128', '', 'cbc', $this->iv);

        mcrypt_generic_init($td, $this->key, $this->iv);
        $decrypted = mdecrypt_generic($td, $code);

        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        
        return utf8_encode(trim($decrypted));
    }

    protected function hex2bin($hexdata) {
        $bindata = '';

        for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
        }

        return $bindata;
    }

}