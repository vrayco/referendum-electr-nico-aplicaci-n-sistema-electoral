<?php

namespace AppBundle\Utils;


use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;

class APISistemaGeneralService
{
    const PRUEBA        = "/api/private/v1/evento-electoral/%s/convocar.json";
    const VALIDAR_CV    = "/api/private/v1/validar/cv.json";

    const MAXIMO_INTENTOS = 15;

    private $container;
    private $apiUrl;

    public function __construct(Container $container, $apiUrl)
    {
        $this->container    = $container;
        $this->apiUrl       = $apiUrl;
    }

    public function getPrueba($datos)
    {

        $url = $this->apiUrl . sprintf(self::PRUEBA, "111");

        try {
            $ch = curl_init($url);
            if (FALSE === $ch)
                throw new Exception(curl_error($ch), curl_errno($ch));

            curl_setopt_array($ch, array(
                CURLOPT_SSL_VERIFYHOST  => false,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_POST            => FALSE,
                CURLOPT_RETURNTRANSFER  => TRUE,
                CURLOPT_HTTPHEADER      => array(
                    //'Authorization: '.$authToken,
                    'Content-Type: application/json'
                ),
                //CURLOPT_POSTFIELDS => json_encode($postData)
            ));

            // Send the request
            $response = curl_exec($ch);

            if (FALSE === $response)
                throw new Exception(curl_error($ch), curl_errno($ch));
            
            $json = json_decode($response);

            return $json;

        } catch(Exception $e) {

            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);

            return false;
        }
    }

    /*
     * EL CV PUEDE NO SER VALIDO POR:
     *      - CV NO VALIDO
     *      - EL PROCESO ELECTORAL NO ESTA ABIERTO
     *      - EL ELECTOR NO ESTA REGISTRADO EN EL SISTEMA
     *      - EL ELECTOR NO ESTA CORRECTAMENTE VALIDADO
     */
    public function validarCV($cv, $l1, $intentos = 0)
    {
        if($intentos < self::MAXIMO_INTENTOS) {
            $url = $this->apiUrl . sprintf(self::VALIDAR_CV);

            $postData = array(
                'cv' => $cv,
                'l1' => $l1,
            );
            
            try {
                $ch = curl_init($url);
                if (FALSE === $ch)
                    throw new Exception(curl_error($ch), curl_errno($ch));

                curl_setopt_array($ch, array(
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        //'Authorization: '.$authToken,
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postData)
                ));

                // Send the request
                $response = curl_exec($ch);

                if (FALSE === $response)
                    throw new Exception(curl_error($ch), curl_errno($ch));

                switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                    case Response::HTTP_OK:
                        $json = json_decode($response);
                        return $json->evento_id;
                        break;
                    case Response::HTTP_BAD_REQUEST:
                        return false;
                    case Response::HTTP_CONFLICT:
                        return $this->validarCV($cv, $l1, $intentos++);
                    default:
                        return false;
                }
            } catch (Exception $e) {

                trigger_error(sprintf(
                    'Curl failed with error #%d: %s',
                    $e->getCode(), $e->getMessage()),
                    E_USER_ERROR);

                return false;
            }
        } else
            return false;
    }

}