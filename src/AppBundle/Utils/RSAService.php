<?php

namespace AppBundle\Utils;

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class RSAService
{
    const FILENAME_PRIVATE_KEY  = "private.key";
    const FILENAME_PUBLIC_KEY   = "public.key";
    const RELATIVA_PATH         = "Resources/RSA";

    private $rootDir;

    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
    }

    public function OpenSSLRSAPublicKeytoXMLSignature ($key) {
        preg_match("/OpenSSLRSAPublicKey[{]modulus=(\w+),publicExponent=(\w+)/", $key, $output_array);

        // falta chequear errores


        $resultado  = "<RSAKeyValue>";
        $resultado .= "<Modulus>" . $output_array[1] . "</Modulus>";
        $resultado .= "<Exponent>" . $output_array[2] . "</Exponent>";
        $resultado .= "</RSAKeyValue>";

        return $resultado;
    }

    public function publicEncrypt($data, $publicKey)
    {
        // Check clave pública              <----- falta por hacer

        $secret = null;
        $key = openssl_get_publickey($publicKey);
        openssl_public_encrypt($data,$secret,$key,OPENSSL_PKCS1_PADDING);

        return base64_encode($secret);
    }

    public function publicDecrypt($data, $publicKey)
    {
        // Check clave pública              <----- falta por hacer

        $key = openssl_get_publickey($publicKey);

        //$data = trim(preg_replace('/\n/', ' ', $data));
        $data = str_replace(PHP_EOL, '', $data);
        $data = str_replace("\\n","",$data);
        $data = str_replace("&#10;", '', $data);
        $data = base64_decode($data);

        openssl_public_decrypt($data, $mensaje, $key, OPENSSL_PKCS1_PADDING);

        return $mensaje;
    }

    public function privateEncrypt($data, $privateKey)
    {
        // Check clave pública              <----- falta por hacer

        $secret = null;
        $key = openssl_get_privatekey($privateKey);
        openssl_private_encrypt($data,$secret,$key,OPENSSL_PKCS1_PADDING);

        return base64_encode($secret);
    }

    public function privateDecrypt($data, $privateKey)
    {
        $key = openssl_get_privatekey($privateKey);
        $data = base64_decode($data);
        $resultado = openssl_private_decrypt($data, $mensaje, $key, OPENSSL_PKCS1_PADDING);

        return $mensaje;
    }

    public function checkPublicKey($key)
    {
        $key = openssl_get_publickey($key);
        if($key)
            return true;
        else
            return false;
    }

    public function generarClaves()
    {
        // Genero un par de claves
        $config = array(
            "digest_alg"        => "sha512",
            "private_key_bits"  => 1024,
            "private_key_type"  => OPENSSL_KEYTYPE_RSA,
        );

        // Create the private and public key
        $res = openssl_pkey_new($config);

        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);

        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);
        $pubKey = $pubKey["key"];

        $fs = new Filesystem();
        try {
            // Si existe la clave privada, la elimino
            if($fs->exists($this->getFilenamePrivateKey())) {
                $fs->remove($this->getFilenamePrivateKey());
            }
            // Si existe la clave publica, la elimino
            if($fs->exists($this->getFilenamePublicKey())) {
                $fs->remove($this->getFilenamePublicKey());
            }

            $fs->dumpFile($this->getFilenamePrivateKey(), $privKey);
            $fs->dumpFile($this->getFilenamePublicKey(), $pubKey);

            return true;

        } catch (IOExceptionInterface $e) {
            return false;
        }
    }

    public function getPublicKey()
    {
        $fs = new Filesystem();

        try {
            // Si no existe la clave publica o la clave privada las genero
            if(!$fs->exists($this->getFilenamePrivateKey()) or !$fs->exists($this->getFilenamePublicKey())) {
                if(!$this->generarClaves())
                    return false;
            }

            $finder = new Finder();
            $finder->files()
                ->in($this->rootDir . '/' . self::RELATIVA_PATH .'/')
                ->name(self::FILENAME_PUBLIC_KEY)
            ;

            $publicKey = null;
            foreach ($finder as $file) {
                $publicKey = $file->getContents();
            }

            return $publicKey;

        } catch (IOExceptionInterface $e) {
            return false;
        }
    }
    
    public function getPrivateKey()
    {
        $fs = new Filesystem();

        try {
            // Si no existe la clave publica o la clave privada las genero
            if(!$fs->exists($this->getFilenamePrivateKey()) or !$fs->exists($this->getFilenamePublicKey())) {
                if(!$this->generarClaves())
                    return false;
            }

            $finder = new Finder();
            $finder->files()
                ->in($this->rootDir . '/' . self::RELATIVA_PATH .'/')
                ->name(self::FILENAME_PRIVATE_KEY)
            ;

            $privateKey = null;
            foreach ($finder as $file) {
                $privateKey = $file->getContents();
            }

            return $privateKey;

        } catch (IOExceptionInterface $e) {
            return false;
        }
    }

    private function getFilenamePrivateKey()
    {
        return $this->rootDir . '/' . self::RELATIVA_PATH .'/' . self::FILENAME_PRIVATE_KEY;
    }

    private function getFilenamePublicKey()
    {
        return $this->rootDir . '/' . self::RELATIVA_PATH .'/' . self::FILENAME_PUBLIC_KEY;
    }
}
