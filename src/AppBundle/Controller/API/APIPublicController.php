<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\EventoElectoral;
use AppBundle\Entity\Voto;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class APIPublicController extends FOSRestController
{
    /**
     * Devuelve la clave pública RSA del sistema.<br/>
     *
     * @ApiDoc(
     * description = "Devuelve la clave pública RSA del sistema.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors"
     * }
     * )
     *
     * @Get("/clave-publica/servidor")
     *
     * @param Request $request the request object
     * @param string     $dni      Dni
     *
     *
     * @return View
     */
    public function getClavePublicaAction() {

        $view = View::create();

        $data = array(
            'public_key' => $this->get('rsa')->getPublicKey()
        );

        $view->setData($data)->setStatusCode(200);

        return $view;
    }

    /**
     * Recibo el voto desde app.<br/>
     *
     * @ApiDoc(
     * description = "Recibo el voto desde app.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 400 = "Returned when the form has errors"
     * }
     * )
     *
     * @Post("/voto")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="m", nullable=false, strict=true, description="Mensaje cifrado. M = V'+ L1 + CV")
     *
     *
     * @return View
     */
    public function postVotoAction(ParamFetcher $paramFetcher)
    {

        $data = $paramFetcher->get('m');
        $json = json_decode($data);

        if(!isset($json->key) or !isset($json->iv) or !isset($json->m))
        {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("El voto no tiene el formato correcto.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $keyCifrada = $json->key;
        $iv         = $json->iv;
        $mCifrado   = $json->m;

        // Obtengo la key (esta cifrada con la clave publica del Sistema Electoral)
        $priKey = $this->get('rsa')->getPrivateKey();
        $key = $this->get('rsa')->privateDecrypt($keyCifrada,$priKey);


        // Descifro $m
        $mcrypt = $this->get('mcrypt');
        $mcrypt->setKey($key);
        $mcrypt->setIV($iv);
        
        $m = $this->get('mcrypt')->decrypt($mCifrado);

        $json = json_decode($m);

        if(!isset($json->cv) or !isset($json->l1) or !isset($json->v_prima))
        {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("El voto no tiene el formato correcto.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $cv     = $json->cv;
        $l1     = $json->l1;
        $vPrima = $json->v_prima;

        $voto = new Voto();
        $voto->setCv($cv);
        $voto->setL1($l1);
        $voto->setVPrima(json_encode($vPrima));

        $errors = $this->get('validator')->validate($voto);
        if(count($errors) > 0) {
            $error = "";
            if($errors[0])
                if($errors[0]->getMessage())
                    $error = (string) $errors[0]->getMessage();
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => $error
            );

            $logger = $this->get('monolog.logger.auditoria');
            $logger->info(
                sprintf("[VOTO RECIBIDO NO VÁLIDO] %s    cv=%s    L1= %s", $error, $cv, $l1)
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $eventoId = $this->get('api_sistema_general')->validarCV($cv, $l1);
        if(!$eventoId) {
            $logger = $this->get('monolog.logger.auditoria');
            $logger->info(
                sprintf("[VOTO RECIBIDO NO VÁLIDO] cv=%s    L1= %s", $cv, $l1)
            );

            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("El voto no ha sido aceptado.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $em = $this->getDoctrine()->getManager();
        $evento = $em->getRepository('AppBundle:EventoElectoral')->find($eventoId);
        if(!$evento) {
            $logger = $this->get('monolog.logger.auditoria');
            $logger->info(
                sprintf("[VOTO RECIBIDO NO VÁLIDO] No existe el evento con id=%s", $eventoId)
            );

            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("El voto no ha sido aceptado.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $voto->setEventoElectoral($evento);
        $em->persist($voto);
        $em->flush();

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf("[VOTO EN ESPERA DE VALIDACION] cv=%s    L1= %s", $cv, $l1)
        );
        
        $view = View::create();

        $view->setData(json_encode(array("mensaje" => "Voto recibido correctamente, en espera de validación.")))->setStatusCode(Response::HTTP_OK);

        return $view;
    }

    /**
     * Devuelve las papelas cifradas y l1s del evento electoral.<br/>
     *
     * @ApiDoc(
     * description = "Devuelve las papelas cifradas y l1s del evento electoral.",
     * statusCodes = {
     * 200 = "Returned when successful",
     * 404 = "Evento Electoral no existe"
     * }
     * )
     *
     * @GET("/evento-electoral/{id}/papeletas-y-l1s")
     *
     * @return View
     */
    public function getPapeletasYL1sAction(EventoElectoral $evento)
    {

        if($evento->getEstado() !== EventoElectoral::ESTADO_FINALIZADO)
        {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf("El evento no ha finalizado.")
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $data = array(
            'papeletas' => $evento->getPapeletas(),
            'l1s'       => $evento->getL1s(),
        );

        $view = View::create();

        $view->setData($data)->setStatusCode(Response::HTTP_OK);

        return $view;
    }
    
}
