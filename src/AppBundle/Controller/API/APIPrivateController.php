<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\EventoElectoral;
use AppBundle\Entity\Voto;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class APIPrivateController extends FOSRestController
{
    /**
     * Convocar evento electoral: El Sistema General comunica al Sistema Electoral la convocatoria de un evento electoral
     *
     * @ApiDoc(
     * description = "Convocar evento electoral: El Sistema General comunica al Sistema Electoral la convocatoria de un evento electoral.",
     * statusCodes = {
     * 201 = "Returned when successful",
     * 400 = "Los parametros no son correctos"
     * }
     * )
     *
     * @Post("/evento-electoral/convocar")
     * 
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="id", nullable=false, strict=true, description="Evento id.")
     * @RequestParam(name="nombre", nullable=false, strict=true, description="Nombre del evento.")
     *
     * @return View
     */
    public function postConvocarEventoElectoralAction(ParamFetcher $paramFetcher) {

        $data = array(
            'id'            => $paramFetcher->get('id'),
            'nombre'        => $paramFetcher->get('nombre'),
        );

        $entity = new EventoElectoral();
        $entity->setId($data['id']);
        $entity->setNombre($data['nombre']);
        $entity->setConvocado(new \DateTime('now'));

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if(count($errors) > 0) {
            $error = "";
            if($errors[0])
                if($errors[0]->getMessage())
                    $error = (string) $errors[0]->getMessage();
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => $error
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }


        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        $logger = $this->get('monolog.logger.auditoria');

        $logger->info(
            sprintf("[EVENTO %s] El Sistema General ha convocado el evento: %s", $entity->getId(), $entity->getNombre())
        );

        $view = View::create();
        $view->setData($entity)->setStatusCode(Response::HTTP_CREATED);

        return $view;
    }

    /**
     * Apertura evento electoral: El Sistema General comunica al Sistema Electoral la apertura del proceso electoral
     *
     * @ApiDoc(
     * description = "Apertura evento electoral: El Sistema General comunica al Sistema Electoral la apertura del proceso electoral.",
     * statusCodes = {
     * 202 = "El proceso electoral ha sido abierto",
     * 400 = "Los parametros no son correctos"
     * }
     * )
     *
     * @Post("/evento-electoral/apertura")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="id", nullable=false, strict=true, description="Evento id.")
     *
     * @return View
     */
    public function postAperturaEventoElectoralAction(ParamFetcher $paramFetcher)
    {

        $id = $paramFetcher->get('id');

        $em = $this->getDoctrine()->getManager();
        $evento = $em->getRepository('AppBundle:EventoElectoral')->find($id);

        if(!$evento) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('El evento con id=%s no ha sido convocado.', $id)
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if($evento->getEstado() != EventoElectoral::ESTADO_CONVOCADO) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No se puede ABRIR el evento electoral id=%s porque su estado es ABIERTO ó CERRADO', $id)
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $evento->setEstado(EventoElectoral::ESTADO_ABIERTO);
        $evento->setInicio(new \DateTime('now'));
        $em->flush();

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf("[EVENTO %s] El Sistema General ha abierto el evento: %s", $evento->getId(), $evento->getNombre())
        );

        $view = View::create();
        $view->setData($evento)->setStatusCode(Response::HTTP_ACCEPTED);

        return $view;
    }

    /**
     * Cierre evento electoral: El Sistema General comunica al Sistema Electoral el cierre del proceso electoral
     *
     * @ApiDoc(
     * description = "Cierre evento electoral: El Sistema General comunica al Sistema Electoral el cierre del proceso electoral.",
     * statusCodes = {
     * 202 = "El proceso electoral ha sido cerrado",
     * 400 = "Los parametros no son correctos"
     * }
     * )
     *
     * @Post("/evento-electoral/cierre")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="id", nullable=false, strict=true, description="Evento id.")
     *
     * @return View
     */
    public function postCierreEventoElectoralAction(ParamFetcher $paramFetcher)
    {

        $id = $paramFetcher->get('id');

        $em = $this->getDoctrine()->getManager();
        $evento = $em->getRepository('AppBundle:EventoElectoral')->find($id);

        if(!$evento) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('El evento con id=%s no ha sido convocado.', $id)
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        if($evento->getEstado() != EventoElectoral::ESTADO_ABIERTO) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No se puede CERRAR el evento electoral id=%s porque su estado no es ABIERTO', $id)
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $evento->setEstado(EventoElectoral::ESTADO_CERRADO);
        $evento->setFin(new \DateTime('now'));
        $em->flush();

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf("[EVENTO %s] El Sistema General ha cerrado el evento: %s", $evento->getId(), $evento->getNombre())
        );

        $view = View::create();
        $view->setData($evento)->setStatusCode(Response::HTTP_ACCEPTED);

        return $view;
    }

    /**
     * Respuesta validar CV. Confirmación de validación del voto
     *
     * @ApiDoc(
     * description = "Confirmación de validación del voto.",
     * statusCodes = {
     * 202 = "Ok, voto validado",
     * 400 = "Los parametros no son correctos"
     * }
     * )
     *
     * @Post("/validar/voto")
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="cv", nullable=false, strict=true, description="CV del elector")
     * @RequestParam(name="l1", nullable=false, strict=true, description="L1")
     * @RequestParam(name="l1s", nullable=true, strict=true, description="L1 de votos anteriores")
     * @RequestParam(name="votaciones", nullable=false, strict=true, description="Lista de id de las votaciones en las que el elector puede participar para el evento electoral")
     *
     * @return View
     */
    public function respuestaValidarCVAction(ParamFetcher $paramFetcher)
    {
        $cv         = $paramFetcher->get('cv');
        $l1         = $paramFetcher->get('l1');
        $l1s        = $paramFetcher->get('l1s');
        $votaciones = $paramFetcher->get('votaciones');

        $em = $this->getDoctrine()->getManager();
        $voto = $em->getRepository('AppBundle:Voto')->findOneBy(array('cv' => $cv, 'l1' => $l1));

        if(!$voto) {
            $data = array(
                "code"      => Response::HTTP_BAD_REQUEST,
                "message"   => sprintf('No existe el voto en el sistema. CV=%s    L1=%s', $cv, $l1)
            );

            $response = new JsonResponse();
            $response->setStatusCode($data['code']);
            $response->setContent(json_encode($data));

            return $response;
        }

        $voto->setVotaciones($votaciones);
        $voto->setEstado(Voto::ESTADO_VALIDADO);

        $l1sArray = json_decode($l1s);

        foreach ($l1sArray as $a)
        {
            if($a !== $l1) {
                $votoViejo = $em->getRepository('AppBundle:Voto')->findOneBy(array('cv' => $cv, 'l1' => $a));
                if ($votoViejo) {
                    $votoViejo->setEstado(Voto::ESTADO_DESCARTADO);
                    $logger = $this->get('monolog.logger.auditoria');
                    $logger->info(
                        sprintf("[VOTO DESCARTADO] cv=%s    L1= %s", $cv, $a)
                    );
                }
            }
        }

        $em->flush();

        $logger = $this->get('monolog.logger.auditoria');
        $logger->info(
            sprintf("[VOTO VALIDADO] cv=%s    L1= %s", $cv, $l1)
        );

        $view = View::create();
        $view->setData(array('mensaje' => 'Ok, voto validado'))->setStatusCode(Response::HTTP_ACCEPTED);

        return $view;
    }
}
