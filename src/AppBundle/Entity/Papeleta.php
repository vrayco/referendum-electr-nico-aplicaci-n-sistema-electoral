<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 *
 * @ORM\Table(name="papeleta")
 * @ORM\Entity()
 * @ExclusionPolicy("all")
 */
class Papeleta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="vPrima", type="text")
     * @Expose
     */
    private $vPrima;

    /**
     * @ORM\Column(name="votaciones", type="json_array", nullable=true)
     * @Expose
     * @Accessor(getter="getVotacionesArray")
     */
    private $votaciones;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EventoElectoral", inversedBy="papeletas")
     * @ORM\JoinColumn(name="evento_electoral_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $eventoElectoral;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vPrima
     *
     * @param string $vPrima
     *
     * @return Papeleta
     */
    public function setVPrima($vPrima)
    {
        $this->vPrima = $vPrima;

        return $this;
    }

    /**
     * Get vPrima
     *
     * @return string
     */
    public function getVPrima()
    {
        return $this->vPrima;
    }

    /**
     * Set eventoElectoral
     *
     * @param \AppBundle\Entity\EventoElectoral $eventoElectoral
     *
     * @return Papeleta
     */
    public function setEventoElectoral(\AppBundle\Entity\EventoElectoral $eventoElectoral = null)
    {
        $this->eventoElectoral = $eventoElectoral;

        return $this;
    }

    /**
     * Get eventoElectoral
     *
     * @return \AppBundle\Entity\EventoElectoral
     */
    public function getEventoElectoral()
    {
        return $this->eventoElectoral;
    }

    /**
     * Set votaciones
     *
     * @param array $votaciones
     *
     * @return Papeleta
     */
    public function setVotaciones($votaciones)
    {
        $this->votaciones = $votaciones;

        return $this;
    }

    /**
     * Get votaciones
     *
     * @return array
     */
    public function getVotaciones()
    {
        return $this->votaciones;
    }

    public function getVotacionesArray()
    {
        return json_decode($this->votaciones);
    }
}
