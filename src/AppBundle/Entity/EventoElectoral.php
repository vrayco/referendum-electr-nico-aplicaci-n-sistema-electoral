<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Expose;

/**
 * EventoVotacion
 *
 * @ORM\Entity
 * @ORM\Table(name="evento_electoral")
 * @UniqueEntity("id")
 * @ExclusionPolicy("all")
 */
class EventoElectoral
{
    const ESTADO_CONVOCADO  = "CONVOCADO";
    const ESTADO_ABIERTO    = "ABIERTO";
    const ESTADO_CERRADO    = "CERRADO";
    const ESTADO_FINALIZADO = "FINALIZADO";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @Expose()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(message="Por favor, ingrese el nombre")
     * @Expose
     */
    private $nombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="convocado", type="datetime", nullable=true)
     * @Expose
     */
    private $convocado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio", type="datetime", nullable=true)
     * @Expose
     */
    private $inicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="datetime", nullable=true)
     * @Expose
     */
    private $fin;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=16)
     * @Expose()
     */
    private $estado;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Voto", mappedBy="eventoElectoral", cascade={"persist", "remove"})
     */
    private $votos;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Papeleta", mappedBy="eventoElectoral", cascade={"persist", "remove"})
     */
    private $papeletas;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\L1", mappedBy="eventoElectoral", cascade={"persist", "remove"})
     */
    private $l1s;

    public function __construct()
    {
        $this->estado       = self::ESTADO_CONVOCADO;
        $this->votos        = new \Doctrine\Common\Collections\ArrayCollection();
        $this->papeletas    = new \Doctrine\Common\Collections\ArrayCollection();
        $this->l1s          = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return EventoElectoral
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return EventoElectoral
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     *
     * @return EventoElectoral
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;

        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     *
     * @return EventoElectoral
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return EventoElectoral
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add voto
     *
     * @param \AppBundle\Entity\Voto $voto
     *
     * @return EventoElectoral
     */
    public function addVoto(\AppBundle\Entity\Voto $voto)
    {
        $this->votos[] = $voto;

        return $this;
    }

    /**
     * Remove voto
     *
     * @param \AppBundle\Entity\Voto $voto
     */
    public function removeVoto(\AppBundle\Entity\Voto $voto)
    {
        $this->votos->removeElement($voto);
    }

    /**
     * Get votos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVotos()
    {
        return $this->votos;
    }

    /**
     * Add papeleta
     *
     * @param \AppBundle\Entity\Papeleta $papeleta
     *
     * @return EventoElectoral
     */
    public function addPapeleta(\AppBundle\Entity\Papeleta $papeleta)
    {
        $this->papeletas[] = $papeleta;

        return $this;
    }

    /**
     * Remove papeleta
     *
     * @param \AppBundle\Entity\Papeleta $papeleta
     */
    public function removePapeleta(\AppBundle\Entity\Papeleta $papeleta)
    {
        $this->papeletas->removeElement($papeleta);
    }

    /**
     * Get papeletas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPapeletas()
    {
        return $this->papeletas;
    }

    /**
     * Add l1
     *
     * @param \AppBundle\Entity\L1 $l1
     *
     * @return EventoElectoral
     */
    public function addL1(\AppBundle\Entity\L1 $l1)
    {
        $this->l1s[] = $l1;

        return $this;
    }

    /**
     * Remove l1
     *
     * @param \AppBundle\Entity\L1 $l1
     */
    public function removeL1(\AppBundle\Entity\L1 $l1)
    {
        $this->l1s->removeElement($l1);
    }

    /**
     * Get l1s
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getL1s()
    {
        return $this->l1s;
    }

    /**
     * Set convocado
     *
     * @param \DateTime $convocado
     *
     * @return EventoElectoral
     */
    public function setConvocado($convocado)
    {
        $this->convocado = $convocado;

        return $this;
    }

    /**
     * Get convocado
     *
     * @return \DateTime
     */
    public function getConvocado()
    {
        return $this->convocado;
    }
}
