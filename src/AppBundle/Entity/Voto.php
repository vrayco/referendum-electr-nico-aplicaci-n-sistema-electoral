<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Voto
 *
 * @ORM\Table(name="voto")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VotoRepository")
 * @UniqueEntity(
 *     fields={"cv", "l1"},
 *     errorPath="cv",
 *     message="Ya existe un voto con igual cv y L1"
 * )
 */
class Voto
{
    const ESTADO_SIN_VALIDAR    = "SIN_VALIDAR";
    const ESTADO_VALIDADO       = "VALIDADO";
    const ESTADO_DESCARTADO     = "DESCARTADO";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cv", type="string", length=255)
     */
    private $cv;

    /**
     * @var string
     *
     * @ORM\Column(name="l1", type="string", length=255)
     */
    private $l1;

    /**
     * @var string
     *
     * @ORM\Column(name="vPrima", type="text")
     */
    private $vPrima;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=32)
     */
    private $estado;

    /**
     * @ORM\Column(name="votaciones", type="json_array", nullable=true)
     */
    private $votaciones;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EventoElectoral", inversedBy="votos")
     * @ORM\JoinColumn(name="evento_electoral_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $eventoElectoral;

    public function __construct()
    {
        $this->estado = self::ESTADO_SIN_VALIDAR;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cv
     *
     * @param string $cv
     *
     * @return Voto
     */
    public function setCv($cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * Get cv
     *
     * @return string
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * Set l1
     *
     * @param string $l1
     *
     * @return Voto
     */
    public function setL1($l1)
    {
        $this->l1 = $l1;

        return $this;
    }

    /**
     * Get l1
     *
     * @return string
     */
    public function getL1()
    {
        return $this->l1;
    }

    /**
     * Set vPrima
     *
     * @param string $vPrima
     *
     * @return Voto
     */
    public function setVPrima($vPrima)
    {
        $this->vPrima = $vPrima;

        return $this;
    }

    /**
     * Get vPrima
     *
     * @return string
     */
    public function getVPrima()
    {
        return $this->vPrima;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Voto
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set eventoElectoral
     *
     * @param \AppBundle\Entity\EventoElectoral $eventoElectoral
     *
     * @return Voto
     */
    public function setEventoElectoral(\AppBundle\Entity\EventoElectoral $eventoElectoral = null)
    {
        $this->eventoElectoral = $eventoElectoral;

        return $this;
    }

    /**
     * Get eventoElectoral
     *
     * @return \AppBundle\Entity\EventoElectoral
     */
    public function getEventoElectoral()
    {
        return $this->eventoElectoral;
    }

    /**
     * Set votaciones
     *
     * @param array $votaciones
     *
     * @return Voto
     */
    public function setVotaciones($votaciones)
    {
        $this->votaciones = $votaciones;

        return $this;
    }

    /**
     * Get votaciones
     *
     * @return array
     */
    public function getVotaciones()
    {
        return $this->votaciones;
    }
}
