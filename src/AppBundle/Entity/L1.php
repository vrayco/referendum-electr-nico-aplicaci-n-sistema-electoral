<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 *
 * @ORM\Table(name="l1")
 * @ORM\Entity()
 * @ExclusionPolicy("all")
 */
class L1
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @ORM\Column(name="l1", type="string", length=255)
     * @Expose
     */
    private $l1;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EventoElectoral", inversedBy="l1s")
     * @ORM\JoinColumn(name="evento_electoral_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $eventoElectoral;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set l1
     *
     * @param string $l1
     *
     * @return L1
     */
    public function setL1($l1)
    {
        $this->l1 = $l1;

        return $this;
    }

    /**
     * Get l1
     *
     * @return string
     */
    public function getL1()
    {
        return $this->l1;
    }

    /**
     * Set eventoElectoral
     *
     * @param \AppBundle\Entity\EventoElectoral $eventoElectoral
     *
     * @return L1
     */
    public function setEventoElectoral(\AppBundle\Entity\EventoElectoral $eventoElectoral = null)
    {
        $this->eventoElectoral = $eventoElectoral;

        return $this;
    }

    /**
     * Get eventoElectoral
     *
     * @return \AppBundle\Entity\EventoElectoral
     */
    public function getEventoElectoral()
    {
        return $this->eventoElectoral;
    }
}
